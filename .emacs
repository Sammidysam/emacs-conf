;;;; Sam's emacs configuration file
; Sam Craig (Sammidysam)
; Started 30 June 2013

;; Comment style
; Start of block (or title) has two semi-colons.
; Those following have one.
; Titles of documents has four semi-colons to stand out.
; Titles have no end puctuation, sentences in block do.
; This style was completely made by me and is only for my special file.
; Always a space after a semi-colon.

;; Set up package manager
(setq package-archives '(("gnu" . "http://elpa.gnu.org/packages/")
                         ("marmalade" . "http://marmalade-repo.org/packages/")
                         ("melpa" . "http://melpa.milkbox.net/packages/")))

;; Create hook for customization that needs to be after and before turning on packages
(defvar customize-after-init-hook nil
  "Hook called after turn-on-packages is called in after-init-hook.")

(defvar customize-before-init-hook nil
  "Hook called before turn-on-packages is called in after-init hook.")

;; Add all ELPA to the load-path.
(let ((default-directory "~/.emacs.d/elpa/"))
    (setq load-path
          (append
           (let ((load-path (copy-sequence load-path)))
             (normal-top-level-add-subdirs-to-load-path))
           load-path)))

(defun turn-on-packages ()
  "Turns on the packages smart-tabs, auto-indent, and smartparens."
  ;; Installed smart-tabs-insinuate options are 'c, 'c++, 'java, 'javascript, 'cperl, 'python, 'ruby, and 'nxml
  ;; Added options are 'vala, 'csharp
  (smart-tabs-insinuate 'c 'c++ 'java 'python 'ruby 'vala 'csharp)
  (auto-indent-global-mode)
  (smartparens-global-mode 1))

;; Add things to the after-init-hook
(add-hook 'after-init-hook (lambda ()
							 (run-hooks 'customize-before-init-hook)
							 (turn-on-packages)
							 (run-hooks 'customize-after-init-hook)))

;; Backup temporary files in ~/.saves
; Better than increasing the project folder's size.
; Autosaving disabled because it didn't work right.
(defconst emacs-tmp-dir (expand-file-name "~/.saves"))

(setq backup-directory-alist `((".*" . ,emacs-tmp-dir))
	  auto-save-default nil
	  create-lockfiles nil
	  delete-old-versions t)

;; Set yes-or-no prompt to y or n
(fset 'yes-or-no-p 'y-or-n-p)

;; Delete selection when pasting over it
(delete-selection-mode 1)

;; Newline at end of file
; GitHub and compilers like it!
(setq require-final-newline 't)

;; Set good style stuff
; Good tab size, use tabs, etc.
(setq-default tab-width 4
              indent-tabs-mode t
			  c-basic-offset tab-width
			  sh-basic-offset tab-width
              LilyPond-indent-level 4)

(setq c-user-style-list '("libconjugar" "StudyHelper"))

(c-add-style "user"
             `("linux"
               (c-basic-offset . ,tab-width)
			   (c-offsets-alist
				(case-label . +))))

(setq c-default-style "linux")

(add-hook 'python-mode-hook
          (lambda ()
            (setq indent-tabs-mode t
				  tab-width 4
				  python-indent 4
				  auto-indent-newline-function 'newline-and-indent)))

(add-hook 'ruby-mode-hook
		  (lambda ()
			(setq indent-tabs-mode t
				  ruby-indent-tabs-mode t
				  ruby-indent-level 4)))

(add-hook 'html-mode-hook
		  (lambda ()
			(setq sgml-basic-offset 4
				  indent-tabs-mode t)))

(add-hook 'emacs-lisp-mode-hook
          (lambda ()
			(setq indent-tabs-mode nil)))

(add-hook 'csharp-mode-hook
		  (lambda ()
			(c-set-style "C#")))

;; Do not compile SCSS at save.
(setq scss-compile-at-save nil)

;; Auto-indent stuff nicer
; Stop forcing indent level of major modes.
(setq auto-indent-assign-indent-level-variables nil)
(setq auto-indent-mode-untabify-on-yank-or-paste nil)
(setq auto-indent-untabify-on-visit-file nil)
(setq auto-indent-untabify-on-save-file nil)

;; Match parentheses, brackets, braces, etc.
; Set color to gray with white text.
(show-paren-mode 1)
(setq show-paren-delay 0)
(set-face-background 'show-paren-match-face "#383838")
(set-face-foreground 'show-paren-match-face "#ffffff")
(set-face-attribute 'show-paren-match-face nil :weight 'extra-bold)

;; Function relating to customizing something relating to smartparens
(defun create-body (&rest _ignored)
  "Automatically set up a body."
  (newline)
  (indent-according-to-mode)
  (forward-line -1)
  (indent-according-to-mode)
  (end-of-line)
  ;; Re-indent everything to fix problems
  (save-excursion
	(forward-line -1)
	(indent-according-to-mode)
	(forward-line)
	(indent-according-to-mode)
	(forward-line)
	(indent-according-to-mode))
  ;; Indent again
  (indent-according-to-mode))

(defun space-and-space-on-each-side (&rest _ignored)
  "Put a space on each side."
  (insert " ")
  (backward-char 1))

(defun space-on-each-side (&rest _ignored)
  "Insert space, then space-and-space-on-each-side."
  (insert " ")
  (space-and-space-on-each-side))

(defun erb-end (&rest _ignored)
  "Insert <% end %> on line below current line."
  (save-excursion
	(end-of-line)
	(newline)
	(insert "<% end %>")
	(indent-according-to-mode)))

;; There *can* actually be whitespace between "#" and "include".
;; If this function is for use by others, make sure to handle that correctly.
(defun c-in-preprocessor-include-p (&rest _ignored)
  "Sees if the first character of the line is '#', which marks it to be a preprocessor line."
  (save-excursion
	(back-to-indentation)
	(when (string= (buffer-substring-no-properties (point) (+ (point) (length "#include"))) "#include")
	  "t")))

(defun sp-web-mode-is-code-context (id action context)
  (when (and (eq action 'insert)
             (not (or (get-text-property (point) 'part-side)
                      (get-text-property (point) 'block-side))))
    t))

(defun customize-smartparens ()
  "Customizes a part of smartparens.  Adds auto-expansion of bodies."
  (sp-with-modes '(
                   c-mode
                   c++-mode
                   shell-script-mode
                   vala-mode
                   perl-mode
                   java-mode
                   sh-mode
                   csharp-mode
                   json-mode
                   scss-mode
                   js-mode
                   web-mode
                   less-css-mode
                   )
    (sp-local-pair "{" nil :post-handlers '((create-body "RET"))))
  (sp-local-pair 'json-mode "[" nil :post-handlers '((create-body "RET")))
  
  (sp-with-modes '(
				   c-mode
				   c++-mode
				   csharp-mode
                   web-mode
				   )
	(sp-local-pair "/*" "*/"
				   :unless '(sp-in-string-p)
				   :post-handlers '(
									(space-and-space-on-each-side "SPC")
									(create-body "RET")
									)))
  (sp-with-modes '(
                   c-mode
                   c++-mode
                   )
	(sp-local-pair "<" ">"
				   :when '(c-in-preprocessor-include-p)))
  
  (sp-with-modes '(
				   markdown-mode
				   gfm-mode
				   )
	(sp-local-pair "*" "*")
	(sp-local-tag "M-*" "**" "**")
	(sp-local-pair "_" "_")
	(sp-local-tag "M-_" "__" "__"))
  
  (sp-with-modes '(web-mode)
    (sp-local-pair "%" "%"
                   :unless '(sp-in-string-p sp-point-after-word-p sp-in-comment-p)
                   :post-handlers '(
                                    (space-and-space-on-each-side "SPC")
                                    (space-on-each-side "=" "#")
                                    ))
    (sp-local-pair "<" nil :when '(sp-web-mode-is-code-context)))
  
  (sp-local-pair 'LilyPond-mode "{" nil :post-handlers '((space-and-space-on-each-side "SPC") (create-body "RET"))))

(add-hook 'customize-after-init-hook (lambda ()
									   (require 'smartparens-config)
									   (customize-smartparens)))

;; Get rid of blinking parent
; I can highlight the matching parenthesis, bracket, brace, etc.
(setq blink-matching-paren nil)

;; Line numers
; Add space before and after line number.
(global-linum-mode t)
(setq linum-format " %d ")

(add-hook 'term-mode-hook (lambda ()
                            (linum-mode -1)))

;; Do not show startup screen
(setq inhibit-startup-screen t)

;; Turn off menu bar, scroll bar, tool bar
; Should get me to fully embrace using the keyboard.
(menu-bar-mode -1)
(scroll-bar-mode -1)
(tool-bar-mode -1)

;; Switch buffers, find files easier
(ido-mode t)

;; Some file associations
(add-to-list 'auto-mode-alist '("\\.gpi\\'" . gnuplot-mode))
(add-to-list 'auto-mode-alist '("\\.gemspec\\'" . ruby-mode))
(add-to-list 'auto-mode-alist '("\\.rake\\'" . ruby-mode))
(add-to-list 'auto-mode-alist '("\\Rakefile\\'" . ruby-mode))
(add-to-list 'auto-mode-alist '("\\Gemfile\\'" . ruby-mode))
(add-to-list 'auto-mode-alist '("\\.jbuilder\\'" . ruby-mode))
(add-to-list 'auto-mode-alist '("\\.vala$" . vala-mode))
(add-to-list 'auto-mode-alist '("\\.vapi$" . vala-mode))

;; Newline from middle of line
(global-set-key (kbd "<C-return>") (kbd "C-e C-j"))

;; Disable line numbers in org-mode
(add-hook 'org-mode-hook (lambda ()
						   (linum-mode 0)))

;; Add Smart Tabs support
(defun smart-tabs-support ()
  (smart-tabs-add-language-support vala vala-mode-hook
	((c-indent-line . c-basic-offset)
	 (c-indent-region . c-basic-offset)))
  (smart-tabs-add-language-support csharp csharp-mode-hook
	((c-indent-line . c-basic-offset)
	 (c-indent-region . c-basic-offset))))

(add-hook 'customize-before-init-hook (lambda ()
									   (smart-tabs-support)))

;; Comment enter in C-mode
(defun comment-enter (&optional ARG)
  "Newline, then checks if point is in comment, if so insert asterisk at beginning of line."
  (interactive)
  (newline)
  (when (sp-point-in-comment)
	(insert "* ")
	(indent-according-to-mode)))

(add-hook 'c-mode-hook (lambda ()
						 (dolist (dir c-user-style-list)
                           (when (string-match-p (concatenate 'string "/" dir "/") (buffer-file-name))
                             (c-set-style "user")))
						 
						 (local-set-key (kbd "RET") 'comment-enter)
                         
                         (flycheck-mode)))

(add-hook 'c++-mode-hook (lambda ()
                           (flycheck-mode)))

(add-hook 'find-file-hook
		  (lambda ()
			(when (or (string-match-p "\\Gemfile\\'" (buffer-file-name)))
			  (flycheck-mode -1))))

(setq flycheck-less-executable "/usr/lib/node_modules/less/bin/lessc")

;; This is for using a remote-terminal.
(setq explicit-shell-file-name "/bin/bash")
(defun remote-term (new-buffer-name cmd &rest switches)
  (setq term-ansi-buffer-name (concat "*" new-buffer-name "*"))
  (setq term-ansi-buffer-name (generate-new-buffer-name term-ansi-buffer-name))
  (let ((clean-switches (delq 'nil switches)))
    (setq term-ansi-buffer-name (apply 'make-term term-ansi-buffer-name cmd nil clean-switches)))
  (set-buffer term-ansi-buffer-name)
  (term-mode)
  (term-char-mode)
  (term-set-escape-char ?\C-x)
  (switch-to-buffer term-ansi-buffer-name))

;; ANSI Term configuration
(defun my-term ()
  (interactive)
  (if (and (buffer-file-name) (string-match "sftp:host=\\(.+\\),user=\\([^/]+\\)\\(.+\\)" (buffer-file-name)))
      (let ((host (match-string 1 (buffer-file-name)))
            (user (match-string 2 (buffer-file-name)))
            (directory (match-string 3 (buffer-file-name)))
            (port-provided (string-match-p ",port=" (match-string 1 (buffer-file-name)))))
        (remote-term host
                     "ssh"
                     (when port-provided
                       "-p")
                     (when port-provided
                       (string-match ",port=\\(.+\\)" host)
                       (match-string 1 host))
                     (concatenate 'string user "@" (if port-provided
                                                       (progn
                                                         (string-match "\\(.+\\),port=.*" host)
                                                         (match-string 1 host))
                                                     host))
                     "-t"
                     (concatenate 'string "cd " (file-name-directory directory) " && $SHELL -l")))
    (ansi-term explicit-shell-file-name)))

;; Web Mode configuration
(add-to-list 'auto-mode-alist '("\\.css\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.js\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.jsonp?\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.[gj]sp\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.handlebars\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))

(setq web-mode-markup-indent-offset 4
      web-mode-css-indent-offset 4
      web-mode-code-indent-offset 4)

(setq web-mode-engines-alist '(("ctemplate" . "\\.html\\'")))
(add-to-list 'auto-mode-alist '("ctemplate" . "\\.html\\'"))

(add-hook 'web-mode-hook
          (lambda ()
            (setq indent-tabs-mode t
                  web-mode-enable-auto-pairing nil)
            (smart-tabs-mode t)
            (local-set-key (kbd "RET") 'comment-enter)))

;; Use solarized-light during the day and solarized-dark at night.
(setq calendar-latitude 40.3994
      calendar-longitude -86.8617
      calendar-location-name "West Lafayette, Indiana")
(add-hook 'customize-after-init-hook
          (lambda ()
            (require 'theme-changer)
            (change-theme 'solarized-light 'solarized-dark)))

(defun indent-file (file)
  (interactive "GWhich file do you want to indent:")
  (print file)
  (if (file-regular-p file)
      (progn
        (find-file file)
        (indent-region (point-min) (point-max))
        (save-buffer)
        (kill-buffer))
    (dolist (loop-file (directory-files file t))
      (when (not (string= (substring (file-name-nondirectory loop-file) 0 1) "."))
        (indent-file loop-file)))))

(defun mute-bell ()
  (interactive)
  (setq ring-bell-function 'ignore))

;; Set correct height
(set-face-attribute 'default nil :height 100)

(setq server-socket-dir "/tmp/emacs-shared")

;; Auto-insertion
; Remove the default auto-insert-alist because it has stupid stuff.
(auto-insert-mode)
(setq auto-insert-directory "~/.insert/"
	  auto-insert-query nil
	  auto-insert-alist nil)

;; Auto-insert-mode templates
(define-auto-insert
  '("\\.\\([Hh]\\|hh\\|hpp\\)\\'" . "C / C++ header")
  ; Translates filename into header name, stores in variable `str'.
  '((upcase (concat (file-name-nondirectory
					 (file-name-sans-extension buffer-file-name))
					"_"
					(file-name-extension buffer-file-name)))
	"#ifndef " str \n
	"#define " str \n \n
	_ \n \n "#endif"))

(define-auto-insert
  '("\\.\\([Cc]\\|cc\\|cpp\\)\\'" . "C / C++ program")
  ; Set str to nil, as no str variable is needed.
  '(nil
	"#include \""
	; Sets stem to the full path of the new file minus the extension.
	(let ((stem (file-name-sans-extension buffer-file-name)))
	  ; Checks if header files exist.
										; If one does, will put the stem minus directory plus extension into file.
	  (cond ((file-exists-p (concat stem ".h"))
			 (file-name-nondirectory (concat stem ".h")))
			((file-exists-p (concat stem ".hpp"))
			 (file-name-nondirectory (concat stem ".hpp")))
			((file-exists-p (concat stem ".H"))
			 (file-name-nondirectory (concat stem ".H")))
			((file-exists-p (concat stem ".hh"))
			 (file-name-nondirectory (concat stem ".hh")))))
	; If one of the conditions succeeded, put a quotation mark into the file.
	; If none succeeded, delete the last 10 characters of the file (everything put in).
	& ?\" | -10))

;; Custom layout creation function
(defun create-shell-layout ()
  "Creates my shell layout used in programming."
  (interactive)
  (split-window-horizontally)
  (other-window 1)
  (my-term)
  (set-window-dedicated-p (selected-window) t)
  (split-window-vertically)
  (my-term)
  (set-window-dedicated-p (selected-window) t)
  (other-window 1))

(defun create-homework-layout ()
  "Creates my layout used when doing homework."
  (interactive)
  (split-window-vertically)
  (other-window 1)
  (my-term)
  (set-window-dedicated-p (selected-window) t)
  (shrink-window (/ (window-height (next-window)) 2))
  (other-window 1))

(defun create-git-layout ()
  "Creates my Git layout used in programming."
  (interactive)
  (split-window-horizontally)
  (other-window 1)
  (magit-status (file-name-directory (buffer-file-name)) 'switch-to-buffer)
  (split-window-vertically)
  (my-term)
  (set-window-dedicated-p (selected-window) t)
  (set-window-dedicated-p (next-window) t)
  (other-window 2))

;; Make C-x C-c prompt me about quitting
(defun ask-before-closing ()
  "Ask whether or not to close, and then close if y was pressed."
  (interactive)
  (if (y-or-n-p (format "Are you sure you want to exit Emacs? "))
      (save-buffers-kill-emacs)
    (message "Canceled exit")))

;; Set key binding for creating nice layout
; Comment and uncomment bindings aren't consistent, so create new bindings.
(global-set-key [f7] 'create-shell-layout)
(global-set-key [f8] 'create-homework-layout)
(global-set-key [f9] 'create-git-layout)
(global-set-key (kbd "C-/") 'comment-region)
(global-set-key (kbd "C-?") 'uncomment-region)
(global-set-key (kbd "C-x C-c") 'ask-before-closing)

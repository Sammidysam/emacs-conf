read -p "Do you want to install the package \`emacs-gnu-smalltalk\` via yum?" -n 1 -r
echo

if [[ $REPLY =~ ^[Yy]$ ]]
then
	su -c "yum install emacs-gnu-smalltalk"
fi

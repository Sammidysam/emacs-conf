all: ./.emacs ./.emacs.d.tree install.el

clean:
	rm .emacs .emacs.d.tree

# First copies the Emacs configuration,
# then installs all ELPA packages via
# install.el.  The package emacs-gnu-smalltalk
# is installed because it is not on ELPA.
install:
	cp .emacs ~/.emacs
	emacs --script install.el
	./gnu-smalltalk.sh

./.emacs: ~/.emacs
	cp ~/.emacs .emacs

./.emacs.d.tree: $(wildcard ~/.emacs.d/*) $(wildcard ~/.emacs.d/*/*)
	tree ~/.emacs.d > .emacs.d.tree

# Keep the list of packages in install.el up-to-date.
install.el: $(wildcard ~/.emacs.d/elpa/*)
	./update-packages.pl

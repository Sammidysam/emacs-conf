;; The list of packages to install.
(setq package-list '( auto-indent-mode coffee-mode csharp-mode dash epl f findr flycheck flycheck-color-mode-line git-commit-mode git-rebase-mode gitignore-mode gnuplot-mode groovy-mode inf-ruby inflections json-mode json-reformat json-snatcher jump less-css-mode let-alist magit markdown-mode markdown-mode+ pkg-info ruby-compilation s scss-mode smart-tabs-mode smartparens solarized-theme theme-changer vala-mode web-mode yaml-mode))

;; The repositories to use for the packages.
(setq package-archives '(("gnu" . "http://elpa.gnu.org/packages/")
                         ("marmalade" . "http://marmalade-repo.org/packages/")
                         ("melpa" . "http://melpa.milkbox.net/packages/")))

(package-initialize)

;; Fetch package list.
(unless package-archive-contents
  (package-refresh-contents))

(dolist (package package-list)
  (when (not (package-installed-p package))
    (package-install package)))

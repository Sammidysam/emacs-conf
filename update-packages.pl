#! /usr/bin/env perl

use File::HomeDir;
use Tie::File;

# The location of the ELPA packages.
my $root = File::HomeDir->my_home . "/.emacs.d/elpa";

# The list of ELPA packages that are installed.
my @packages = ();

opendir my $dh, $root or die "$0: opendir: $!";

# Read all of the files in $root.
while (defined(my $name = readdir $dh)) {
	next if (! -d "$root/$name") || $name eq "." || $name eq ".." || $name eq "archives";

	# Transfer directory name to package name.
	# A hyphen followed by a number designates the end of the package name.
	$name =~ /-[0-9]/;

	# Add the package name to the array.
	# $-[0] is the beginning position of the regular expression match up above.
	push @packages, substr($name, 0, $-[0]);
}

# Sort packages alphabetically.
@packages = sort { lc($a) cmp lc($b) } @packages;

# Create the final Emacs Lisp code line.
my $result_string = "(setq package-list '(@packages))";

# Write the line to the second line of `install.el`.
tie my @install_file, "Tie::File", "install.el" or die "$0: tie: $!";

$install_file[1] = $result_string;

untie @install_file;
